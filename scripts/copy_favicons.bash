#!/usr/bin/env bash

# ==============================================================================
# title       : copy_favicons.bash
# description : This script copies favicons from the subfolder "_favicons"
#               into the jekyll project root folder.
#               The checks are there to make sure that the parameters given
#               to this script are correct.
# date        : 2019-11-07
# usage       : $ bash ./scripts/copy_favicons.bash "_favicons/" "public/" "/"
# parameters  : First parameter $1 is the source folder name which contains the
#               favicons,
#               second parameter $2 is the target folder name into which the
#               favicons are copied,
#               third parameter $3 is the subpath in target folder which
#               is usually just a slash.
# notes       : The intention to outsource this script is to collect all shell
#               scripts in a separate scripts folder instead of putting all
#               scripts with all its length into the GitLab CI pipeline
#               configuration yaml file.
#               Beside that users can manually call this script as well.
# ==============================================================================

set -e

# Give usage message if number of parameters is not zero or three.
if [[ "$#" -ne 0 ]] && [[ "$#" -ne 3 ]]
then
  echo "copy_favicons.bash:"
  echo "The wrong number of parameters are given."
  echo "Usage: Call script with zero or three parameters:"
  echo "e.g. \$ bash ./scripts/copy_favicons.bash " \
       "\"_favicons/\" \"public/\" \"/\""
  exit 1
fi

# Regex to match folder names with trailing slash.
patternFolderNameWithSlash="^+\/$"

# Checks that variable is set, folder exists and folder name ends with "/".
FAVICONS_SOURCE_DIR="_favicons/"
if [[ -n "${1}" ]] &&
   [[ "${1}" =~ [patternFolderNameWithSlash] ]] &&
   [[ -d "${1}" ]]
then
  FAVICONS_SOURCE_DIR=${1}
fi

# Checks that variable is set, folder exists and folder name ends with "/".
FAVICONS_TARGET_DIR="public/"
if [[ -n "${2}" ]] &&
   [[ "${2}" =~ [patternFolderNameWithSlash] ]] &&
   [[ -d "${2}" ]]
then
  FAVICONS_TARGET_DIR=${2}
fi

# Checks that variable is set, folder exists and sub-path ends with "/"
# but is not "/" itself which then defaults to "" (empty string) for proper
# string concatination later on.
# The parameter of a sub-path is needed for use in GitLab CI pipeline.
FAVICONS_PROJECT_SUBPATH=""
if [[ -n "${3}" ]] &&
   [[ "${3}" =~ [patternFolderNameWithSlash] ]] &&
   [[ -d "${3}" ]]
then
  FAVICONS_PROJECT_SUBPATH=${3}
fi

# Copy files to target if source files are given.
if [[ -d ${FAVICONS_SOURCE_DIR} ]]
then
  cp ./"${FAVICONS_SOURCE_DIR}"* \
     ./"${FAVICONS_TARGET_DIR}${FAVICONS_PROJECT_SUBPATH}"
  echo "copy_favicons.bash:"
  echo "Copying favicons completed."
  exit 0
else
  echo "copy_favicons.bash:"
  echo "Favicons folder is missing."
  exit 1
fi
