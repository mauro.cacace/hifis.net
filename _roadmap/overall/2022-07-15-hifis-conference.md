---
date: 2022-07-15
title: Tasks in July 2022
service: overall
---

## HIFIS Conference
Until summer, HIFIS will give all stakeholders the opportunity to strengthen our community and to contribute to the further development of HIFIS, especially in light of the upcoming review.
As a part of this, HIFIS will organize an All-HIFIS conference in Berlin, possibly as presence meeting, if circumstances allow.
