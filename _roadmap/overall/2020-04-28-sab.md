---
date: 2020-04-28
title: Tasks in April 2020
service: overall
---

## First meeting of HIFIS Scientific Advisory Board

The HIFIS Scientific Advisory Board gathered in a video conference to discuss the progress of HIFIS and provide advice. Comments and suggestions were compiled in a comprehensive report.
