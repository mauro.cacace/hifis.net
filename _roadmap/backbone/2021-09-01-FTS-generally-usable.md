---
date: 2021-09-01
title: Tasks in September 2021
service: backbone
---

## HIFIS data transfer generally usable for interested centres
The HIFIS data transfer service will be improved such that any combination of interested centres can
participate by only needing to install a lightweight client that can manage third party copy.





