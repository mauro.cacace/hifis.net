---
title: <i class="fas fa-external-link-alt"></i> Helmholtz AAI
title_image: markus-spiske-PsRUMc7vilg-unsplash.jpg
layout: services/default
author: none
additional_css:
    - title/service-title-buttons.css
excerpt:
  "Seamless Access to Cloud Services for Helmholtz & Friends."
redirect_to: https://aai.helmholtz.de
---
