---
title: Search in hifis.net
title_image: default
layout: default
excerpt:
    Search in hifis.net
---

{% if site.baseurl != '/' %}
{% assign base_url = site.baseurl %}
{% else %}
{% assign base_url = '' %}
{% endif %}

# Search in hifis.net
{:.text-success}

Type in your keywords.
Maximum number of results shown is 20.

<form role="search">
<div id="form-group">
    <input type="search" id="search-input" placeholder="Search..." class="form-control">
    <ul id="results-container"></ul>
</div>

<script src="{{ base_url }}/js/simple-jekyll-search.js"></script>

<script>
    window.simpleJekyllSearch = new SimpleJekyllSearch({
    searchInput: document.getElementById('search-input'),
    resultsContainer: document.getElementById('results-container'),
    json: '{{ base_url }}/search.json',
    searchResultTemplate: '<li><a href="{url}?query={query}" title="{desc}">{title}</a></li>',
    noResultsText: 'No results found',
    limit: 20,
    fuzzy: false,
    exclude: ['Welcome']
    })
</script>
</form>

For this search functionality, [Simple Jekyll Search](https://github.com/christian-fei/Simple-Jekyll-Search) is used, which is released under [MIT license](https://github.com/christian-fei/Simple-Jekyll-Search/blob/master/LICENSE.md).

## Missing anything?

If you have suggestions, questions, or queries, please don't hesitate to write us.

<a href="{% link contact.md %}" 
                            class="btn btn-outline-secondary"
                            title="HIFIS Helpdesk"> 
                            Contact us! <i class="fas fa-envelope"></i></a>
