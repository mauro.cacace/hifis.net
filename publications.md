---
title: Publications
title_image: default
layout: default
redirect_from:
  - mission/publications.html
  - mission/publications
excerpt:
    Publications on HIFIS.
---
# Publications, Presentations, News

##### 2022

* **Helmholtz Federated IT Services: Innovative Cloud and Software Services for
Science**. In: Software Engineering (SE) 2022, Track rSE 22, February 2022.
* [**HIFIS: VO Federation for EFSS**](https://indico.cern.ch/event/1075584/contributions/4658939/) In [CS3 2022 - Cloud Storage Synchronization and Sharing](https://indico.cern.ch/event/1075584/), January 2022.

##### 2021

* [**HIFIS Survey 2021 Report**](https://hifis.net/news/2021/12/20/survey-report), Blog Post, December 2021.
* [**Solutions for the Ages – a Short Crash Course on Sustainable Software Development**](https://www.hzdr.de/publications/Publ-32579). In: HIDA Annual Conference 2021, November 2021.
* [**Roles in Research Software Engineering (RSE) Consultancies**](https://doi.org/10.5281/zenodo.5530443). In: [Research Software Engineers in HPC Workshop at SC21 (RSE-HPC-2021)](https://us-rse.org/rse-hpc-2021/), November 2021.
* [**HIFIS: VO Federation for EFSS**](https://indico.cern.ch/event/1078853/contributions/4576197/) In: [HEPiX Automn 2021 online Workshop](https://indico.cern.ch/event/1078853/), October 2021
* [**Helmholtz IT Services for Science: HIFIS**](https://indico.cern.ch/event/1078853/contributions/4580117/) In: [HEPiX Automn 2021 online Workshop](https://indico.cern.ch/event/1078853/), October 2021
* [**Helmholtz AAI full presentation**](https://aai.helmholtz.de/news/2021/10/22/helmholtz-aai-full-presentation-in-dfn-meeting.html), in [75. DFN Betriebstagung](https://www.dfn.de/veranstaltungen/bt/infos/to-bt/#c18414), October 2021.
* [**Helmholtz Resonator Podcast on HIFIS**](https://resonator-podcast.de/2021/res172-hifis/). In [Resonator-Podcast](https://resonator-podcast.de/), October 2021
* [**Checkliste zur Unterstützung der Helmholtz-Zentren bei der Implementierung von Richtlinien für nachhaltige Forschungssoftware**](https://doi.org/10.48440%2Fos.helmholtz.031), September 2021.
* [**HIFIS: IT-Services für Helmholtz &amp; Partner**](https://www.dfn.de/fileadmin/5Presse/DFNMitteilungen/DFN_Mitteilungen_99.pdf). In: [DFN Mitteilungen](https://www.dfn.de/publikationen/dfnmitteilungen/), pp. 44--45, June 2021.
* [**Easy Diagram creation in Gitlab**](https://hifis.net/tutorial/2021/06/02/easy-diagram-creation-in-gitlab.html), Blog post, June 2021.
* [**Indikatoren für Open Science: Report des Helmholtz Open Science Forum**](https://doi.org/10.2312/os.helmholtz.014), June 2021.
* [**Solutions for the Ages – a Short Crash Course on Sustainable Software Development**](https://www.hzdr.de/publications/Publ-32579). In: International Virtual Covid Data Challenge 2021, April 2021.
* [**Helmholtz AAI Update**](https://aai.helmholtz.de/news/2021/04/14/helmholtz-aai-in-nfdi-meeting.html). Presentation on the role of Helmholtz AAI in the context of AARC, EOSC, and NFDI, April 2021.
* Workshop material [**Using Containers in Science**](https://hifis.gitlab.io/hifis-workshops/using-containers-in-science/), March 2021.
* [**Helmholtz Federated IT Services**](https://www.openstoragenetwork.org/wp-content/uploads/2021/03/National-and-International-Trends-in-Research-Storage-at-Scale-Concept-Paper-1.pdf). In: National and International Trends in Research Storage at Scale, Open Storage Network Concept Paper, March 2021.
* [**Helmholtz AAI**](http://marcus.hardt-it.de/2103-Helmholtz-AAI). Teaser talk, In: [74. DFN Betriebstagung](https://www.dfn.de/veranstaltungen/bt/infos/to-bt/#c18414), March 2021.
* [**HIFIS transfer service: FTS for Helmholtz**](https://indico4.twgrid.org/indico/event/14/session/10/contribution/46). In: [ISGC 2021 - International Symposium on Grids & Clouds](https://indico4.twgrid.org/indico/event/14/), March 2021.
* [**HIFIS: Sync&Share Federation for Helmholtz**](https://indico.cern.ch/event/970232/contributions/4157924/). In: [CS3 2021 - Cloud Storage Synchronization and Sharing](https://indico.cern.ch/event/970232/program), January 2021.

##### 2020
* **Digitale Dienste für die Wissenschaft - wohin geht die Reise?**, December 2020
  * German version: [10.5281/zenodo.4301924](https://doi.org/10.5281/zenodo.4301924)
  * English version: [10.5281/zenodo.4301947](https://doi.org/10.5281/zenodo.4301947)
* **HIFIS Software Survey 2020 Evaluation**, Blog Post Series, Nov 2020-Jan 2021.
  * [A Community Perspective]({% post_url 2021/01/2021-01-21-survey-results-community %})
  * [Consulting Perspective]({% post_url 2020/12/2020-12-16-survey-results-consulting %})
  * [Programming, CI and VCS]({% post_url 2020/11/2020-11-27-survey-results-language-vcs %})
  * [A Technology Perspective]({% post_url 2020/11/2020-11-27-survey-technology %})
* [**Federated data storage for Helmholtz Research & Friends**](https://www.openstoragenetwork.org/seminar-series/nov-12-2020-national-and-international-trends-in-research-storage-at-scale/). In: National and International Trends in Research Storage at Scale, [Open Storage Network](https://www.openstoragenetwork.org), November 2020.
* HIFIS presentations at [EGI conference](https://indico.egi.eu/event/5000/overview) (2-5 November 2020):
  - [**Helmholtz Federated IT and Accessible Compute Resources for Applied AI Research**](https://indico.egi.eu/event/5000/contributions/14353/).
  - [**HIFIS transfer service: FTS for everyone**](https://indico.egi.eu/event/5000/contributions/14383/).
* [**Presentations** of all major HIFIS working groups](https://events.hifis.net/event/25/timetable/#all.detailed) at the 2nd All-Hands HIFIS Meeting, October 2020.
* [**HIFIS backbone transfer service: FTS for everyone**](https://indico.cern.ch/event/898285/contributions/4041954/). In: [HEPiX Autumn 2020 Online workshop](https://indico.cern.ch/event/898285/), October 2020.
* **Docker For Science**, Blog post series, September 2020:
  * [Part 1: Getting Started with Docker]({% post_url 2020/09/2020-09-23-getting-started-with-docker-1 %})
  * [Part 2: A Dockerfile Walkthrough]({% post_url 2020/09/2020-09-25-getting-started-with-docker-2 %})
  * [Part 3: Using Docker in Practical Situations]({% post_url 2020/09/2020-09-29-getting-started-with-docker-3 %})
* **HIFIS - Platform, Training and Support for a Sustainable Software Development**. In: DLR Wissensaustausch-Workshop Software Engineering (WAW SE VII), September 2020.
* [**Zusammenarbeiten mit der Helmholtz-Cloud**](https://www.helmholtz-berlin.de/media/media/oea/aktuell/print//lichtblick/246/hzb_lichtblick_ag-44_september-2020_extern_web.pdf), Lichtblick Helmholtz Zentrum Berlin, p. 4, September 2020.
* [**HZI-led Coronavirus Sero-Survey** in collaboration with HIFIS](https://www.dzif.de/en/how-many-people-are-now-immune-sars-cov-2), August 2020.
  * [German Version](https://www.dzif.de/de/wie-viele-menschen-sind-heute-schon-immun-gegen-sars-cov-2)
* [**Promoting IT based science at all levels**](https://www.egi.eu/about/newsletters/helmholtz-federated-it-services-promoting-it-based-science-at-all-levels/). In: [EGI Newsletter #37](https://www.egi.eu/news/june-edition-of-the-egi-newsletter/), June 2020.
* [**HIFIS suggestions and guidelines**]({% link services/overall/learning-materials.md %}), Blog post series, May-June 2020.
  * [Collaborative Notetaking]({% post_url 2020/06/2020-06-26-guidelines-for-collaborative-notetaking %})
  * [Video Conferencing]({% post_url 2020/05/2020-05-15-guidelines-for-video-conferencing %})
  * [Chatting]({% post_url 2020/05/2020-05-15-guidelines-for-chatting %})
* [**Summary of the Feedback Report of the Scientific Advisory Board**]({% post_url 2020/05/2020-05-27-sab-summary %}), May 2020.
* [**S/MIME Signing Git Commits**]({% post_url 2020/04/2020-04-15-smime-signing-git-commits %}), Blog post, April 2020.
* [**The HIFIS Cloud Competence Cluster**](https://indico.cern.ch/event/854707/contributions/3680436/). In: [CS3 2020 Workshop on Cloud Services for Synchronisation and Sharing](https://cs3.deic.dk/), January 2020.

##### 2019
* [**Presentations**](https://indico.desy.de/event/23411/attachments/32388/) at the 1st HIFIS Conference, October 2019.
* [**Helmholtz Federated IT Services (HIFIS) – Creating Services together**](https://betterscientificsoftware.github.io/swe-cse-bof/2019-11-sc19-bof/04-haupt-helmholtz.pdf) at [Software Engineering and Reuse in Modeling, Simulation, and Data Analytics for Science and Engineering (SC 2019 BOF)](https://betterscientificsoftware.github.io/swe-cse-bof/2019-11-sc19-bof/)
* [HDF Cloud – Helmholtz Data Federation Cloud Resources at the Jülich Supercomputing Centre](http://jlsrf.org/index.php/lsf/article/view/173)
* [Integrierte Entwicklungs- und Publikationsumgebung für Forschungssoftware und Daten am Helmholtz-Zentrum Dresden-Rossendorf (HZDR)](https://av.tib.eu/media/42518)
* [Anforderungen, Wünsche und Erfahrungen für den Aufbau des HIFIS Competence Clusters "Software Services" in der Helmholtz-Gemeinschaft](https://de-rse.org/de/conf2019/talk/LQDSZW/)
* [Continuous Documentation for Users, Developers and Maintainers](https://zenodo.org/record/3247324)
* [Share and synchronize your data with HZB Cloud Nubes safely](https://www.helmholtz-berlin.de/zentrum/locations/it/datenablage/persoenliche/nextcloud/index_en.html)



# Software Publications

HIFIS creates its own software packages, but also contributes to Open Source software in general.

#### Software Packages

##### 2022

* [Ansible RSD role](https://github.com/hifis-net/ansible-role-rsd) - Ansible role to set up the Research Software Directory.

##### 2021

* [HIFIS Surveyval](https://codebase.helmholtz.cloud/hifis/overall/surveys/hifis-surveyval) - Python Package for analysing survey data.
* [Ansible Netplan Role](https://github.com/hifis-net/ansible-role-netplan) - Ansible role that installs Netplan and configures networking on hosts.
* [Ansible SSH keys role](https://gitlab.com/hifis/ansible/ssh-keys) - Ansible role to distribute authorized SSH public keys to users.

##### 2020

* [Ansible GitLab Role](https://gitlab.com/hifis/ansible/gitlab-role) - Ansible role to install and configure the GitLab Omnibus package, also in a high availability context.
* [Ansible Keepalived Role](https://gitlab.com/hifis/ansible/keepalived-role) - Ansible role that sets up Keepalived for high availabilty.
* [Ansible Redis Role](https://gitlab.com/hifis/ansible/redis-role) - Ansible role for setting up a highly available Redis cluster.
* [Ansible HAProxy role](https://gitlab.com/hifis/ansible/haproxy-role) - Ansible role to set up HAProxy to be used as a load balancer.
* [Ansible GitLab-Runner role](https://github.com/hifis-net/ansible-role-gitlab-runner) - Ansible role for deploying GitLab-Runner (optimized for Openstack).
* [GitLab-Runner for Power](https://gitlab.com/hzdr/gitlab-runner/-/releases) - GitLab-Runner packages for the `ppc64le` architecture.

#### Contributions

##### 2022
* [**Ansible Role Certbot**](https://github.com/geerlingguy/ansible-role-certbot) - [Fix snap symlink task failing in initial dry-run](https://github.com/geerlingguy/ansible-role-certbot/pull/166)
* [**Ansible Role Docker**](https://github.com/geerlingguy/ansible-role-docker) - [Fix docker-compose update](https://github.com/geerlingguy/ansible-role-docker/pull/343)

##### 2021
* [**Gitleaks**](https://github.com/zricethezav/gitleaks) - [Fix example in leaky-repo.toml](https://github.com/zricethezav/gitleaks/pull/559)
* [**Ansible (community.general)**](https://github.com/ansible-collections/community.general) - [Fix HAProxy draining](https://github.com/ansible-collections/community.general/pull/1993) 
* [**Ansible Collection Hardening**](https://github.com/dev-sec/ansible-collection-hardening/) 
  * [Add variable to specify SSH host RSA key size](https://github.com/dev-sec/ansible-collection-hardening/pull/394)
  * [fix galaxy action to update local galaxy.yml](https://github.com/dev-sec/ansible-collection-hardening/pull/395)
* [**Ansible Role Elasticsearch**](https://github.com/elastic/ansible-elasticsearch) - [Stop plugin install to fail in check mode](https://github.com/elastic/ansible-elasticsearch/pull/787)

##### 2020

* [**Python Poetry**](https://python-poetry.org/) - [Respect REUSE spec when including licenses](https://github.com/python-poetry/poetry-core/pull/57)
* [**Ansible (community.crypto)**](https://github.com/ansible-collections/community.crypto) - [openssl_pkcs12 parse action: always changed in check mode](https://github.com/ansible-collections/community.crypto/issues/143)
* [**Ansible Collection Hardening**](https://github.com/dev-sec/ansible-collection-hardening/) - [Use package state 'present' since 'installed' is deprecated](https://github.com/dev-sec/ansible-collection-hardening/pull/168)
