---
title: "HIFIS Survey 2020: A Community Perspective"
date: 2021-01-21
authors:
    - dolling
    - dworatzyk
layout: blogpost
title_image: default
excerpt_separator: <!--more-->
categories:
    - report
tags:
    - survey
    - community
---
Data from 467 respondents of the HIFIS Survey 2020 were analyzed to give an overview of research software developers at 
Helmholtz and their day-to-day work. 
While software development plays a crucial role in all six research domains, only about half of the respondents 
are satisfied with the support at their centers. 
Results of this survey will help us to better understand how HIFIS Software Services could improve this situation.

<!--more-->

## Survey outreach 
In the beginning of 2020, the HIFIS Software Survey was conducted to learn about current software development practices 
at Helmholtz and to assess the demand for specific services to support researchers in their day-to-day work. 
For the community team, it was particularly important to get a better grasp of who develops software and to better 
understand their needs for technological and non-technological support.
Our approach to the data was to draw a picture of the "typical" research software developer, to evaluate 
the support currently provided and to identify additional challenges that should be addressed in our future work.
 
Seventeen of the 19 Helmholtz centers and 3 external researchers participated in this survey, 
providing 467 individual responses. 
With about 42.000 employees within Helmholtz we can say that at least 1% are concerned with software development 
(and also found the time to complete our survey - thank you <3). 
Here is an overview of each center's contribution to our survey and the relative number of Helmholtz employees per center:

![Survey outreach]({{ site.directory.images | relative_url }}/posts/2021-01-15-hifis-survey-results-community/survey-outreach.svg)

As we can, we reached most Helmholtz centers except for two and received responses from each of the six research domains. 
Compared to their relative headcount, some centers contributed over-proportionately to the HIFIS survey. 

Well done, DKFZ!

## Research software made by Helmholtz
Now, let's have a look who stands behind the research software made by Helmholtz:
The median software developer in our survey has 8 years of research experience and 9 years of software development experience;
spends 49.5% of the working time on software development; and uses 3 different programming languages at work.
The most used languages are Python (76.7%), C++ (45.6%), and R (28.3%) 
\- for details see this [blog post on programming languages]({% post_url 2020/11/2020-11-27-survey-results-language-vcs %}).
Software is typically developed in a team with 2-5 other developers for a user base of 2-10 persons.
These results support what has been suspected earlier, namely, that software development is fundamental to researchers' work. 
Indeed, the more time survey participants spent on research related work, the more experienced they were in terms of software development as shown by the figure below. 
The question which role software development plays in research could then be answered: an important one!

![Research, software and support]({{ site.directory.images | relative_url }}/posts/2021-01-15-hifis-survey-results-community/research-software-support.svg)

Given the strong link between software development and research, we also wanted to know whether researchers felt 
sufficiently supported at their research centers regarding software development and training.
The answer was a classic _Jein_. 
While half (50.8%) of the respondents seemed satisfied with the current state, 
the other half of the respondents (49.2%) reported that they needed more support. 
Clearly, we wanted to know which kind of support they needed. So we asked them. 
To find out which support activities they were most interested in and which services we should focus on in the future, 
we first let them choose from a list of predefined services.
As shown in the figure above, training (68.7%), tools (58.1%) and consultation on best practices (52.5%) 
were chosen by the majority of respondents.
In order to get a more complete picture of what could be improved, 
we also included a few open response questions asking respondents about their wishes and suggestions 
for a good software development support and their preferred kind of learning. 
Word and code frequency analysis gave a fairly clear picture of the respondents' needs:

![More than words]({{ site.directory.images | relative_url }}/posts/2021-01-15-hifis-survey-results-community/word-code-cloud.png)
 
As expected, basic and advanced training courses; consulting offerings; and a centrally provided, easily accessible 
infrastructure were at the top of the wish list. Support on software licensing was mentioned as a particularly important
topic. 
An additional aspect, that was not directly addressed by our survey, was the recognition and regulation of sustainable 
and open source development.
It was frequently mentioned by respondents that they experienced a lack of awareness for best practices in software 
development among both colleagues and superiors. 
Providing official guidelines and best practice guides was seen as a basic condition to facilitate software development, while increasing social and professional recognition was discussed as a significant motivator to put such guidelines into use.

 
## Sustainable Software Practices
Now, let's have a look where we currently stand regarding sustainable software development:
To get a first overview to what extent best practices of sustainable software development are already established in the 
Helmholtz Association, our survey included questions about researchers sharing and publishing their own software as well as citing the software of others.

![Sustainable software practices]({{ site.directory.images | relative_url }}/posts/2021-01-15-hifis-survey-results-community/sustainable-software-practices.svg)

Results show that most respondents share their code regularly with the own research group, 
sometimes with their research organization or research community, but never with the general public.
They publish their code regularly on internal platforms or public platforms like GitHub but never on a 
research repository or a software package index.
Code comments, README files and installation instructions are regularly provided by most respondents; user manuals, 
requirement specification, technical documentation, and release notes sometimes; whereas contributor guides and developer 
guides are almost never provided. 
Considering the many uncertainties regarding software licenses described in the free text section, it is not surprising 
that almost 20% of the respondents stated that they usually do not license their code.
Finally, we wanted to know whether respondents reference software in research publications - yes, 82.0% do using the 
name (78.7%), link (72.6%) or doi (54.8%) of the software - and what they do to make it easier to cite their software in 
a research publication. 
Surprisingly, only 45.0% take care of the citability of their software, of which the vast majority (96.6%) uses a citation 
hint in the README file and a few respondents (11.3%) use a separate citation file. 

## Outlook
In addition to providing infrastructure and formal training or consultation in different areas of software development, 
it is one goal of HIFIS Software Services is to support the building of a strong community that promotes and commits to 
common standards of sustainable software development and to help to improve the re-usability, maintainability, and
extendability of research software. 
As this survey shows, we are on the right track (more or less) but we are not there yet.
It seems in particular that more work is needed in regard to software licensing. 
Moreover, we would like to see the communication and knowledge exchange across Helmholtz centers improved. 
So, join us for one of the next [Hacky Hour]({% link events.md %}) sessions and share your code, your story, 
your expertise, or your opinion because:

Sharing is caring!
