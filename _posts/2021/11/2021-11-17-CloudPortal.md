---
title: "Release of Helmholtz Cloud Portal"
title_image: background-cloud.jpg
date: 2021-11-17
authors:
  - spicker
layout: blogpost
categories:
  - milestone
excerpt: >
  The production version of the Helmholtz Cloud Portal is released and will be continuously developed from now on. With some new features and a stable performance from the beginning, the beta phase can now be finished.  

---

#### Helmholtz Cloud Portal
The production version of the [Helmholtz Cloud Portal](https://helmholtz.cloud/) is released and will be continuously developed from now on. With some new features and a stable performance from the beginning, the beta phase can now be finished. The Portal allows harmonized access to all Helmholtz Cloud services. It contains all necessary information to access the available services. 

#### Features
* The new search function and filter options help to find a suitable service. 
* The availability of the service is tested automatically and is visible in the service card. 
* The service description contains all main information such as support, service level and limitations. 
* User comments are welcome in the new free text field and will help to continuously improve the usability of the portal.

#### Helmholtz Cloud Services
The Helmholtz Cloud Portal mediates the user's access to all Helmholtz Cloud Services. The portfolio is continuously being updated. The current status can be found [here](https://hifis.net/doc/service-portfolio/service-portfolio-management/current-services-in-portfolio/).

Next upcoming services will presumably be:

* LimeSurvey by HMGU
* Events Management platform by DESY (already usable via direct link [events.hifis.net](https://events.hifis.net/))
* Rancher managed Kubernetes by DESY
* JupyterHub by DESY

#### Information about Helmholtz Cloud
In the Helmholtz Cloud, members of the Helmholtz Association of German Research Centres provide selected IT-Services for joint use.
The Service Portfolio covers the whole scientific process and offers Helmholtz employees and their project partners a federated community cloud with uniform access for them to conduct and support excellent science. [Read more!]({% link services/cloud/Helmholtz_cloud.md %})

#### Comments and Suggestions

If you have suggestions or questions, please do not hesitate to contact us.

<a href="{% link contact.md %}"
                            class="btn btn-outline-secondary"
                            title="HIFIS Helpdesk">
                            Contact us! <i class="fas fa-envelope"></i></a>


