---
title: Example Blog Post Template
date: 2019-10-29
authors:
  - hueser
  - erxleben
layout: blogpost
title_image: default
categories:
  - blog
  - tutorial
tags:
  - example
  - template
  - markdown
additional_js:
  - mathjax.js
excerpt:
  This post will give you an overview of all the features you can use in your
  very own HIFIS Software blog post.
---

# Blog Post Metadata

This post will give you an overview of all the features you can use in your
very own HIFIS Software blog post.

## The Frontmatter

At the top of your blog post you need to enter the so called _Jekyll_
_frontmatter_ which defines the meta-information about your blog post.

Note that the frontmatter is placed at the very top of your markdown file
and begins and ends with `---`.

For example:
```
---
title: A Sample Blog Post Title
date: 2019-12-31
authors:
  - example_author_id
layout: blogpost
title_image: default
excerpt_separator: <!--more-->
categories:
  - hifis
tags:
  - draft
additional_css:
  - my_own.css
additional_js:
  - mathjax.js
---
```

Here is an overview over all the variables that can appear in your frontmatter:

|        Variable       | Mandatory | Default Value |
|-----------------------|:---------:|:-------------:|
| **title**             | Yes       | -             |
| **date**              | Yes       | -             |
| **authors**           | Yes       | -             |
| **layout**            | Yes       | blogpost      |
| **title_image**       | No        | default       |
| **excerpt_separator** | No        | `<!--more-->` |
| **categories**        | No        |  -            |
| **tags**              | No        |  -            |
| **additional_css**    | No        |  -            |

## Variables Explained in Depth

If you do not want to set any of the optional variables it is safe to leave
them out completely.

### title

Set the title of the blog post.
Prefer a short, on-point version, it has to fit in the pages' header among other
things.

### date

Set the publication date of the blog post.
Please use a date format according to this example given:
`YYYY-MM-DD`, e.g. `2019-10-29`.

### authors

Give a list of author IDs in YAML syntax.
You can find your author ID in the YAML-file `_data/hifis_team.yml`.
If your name is not contained you can add it by editing the hifis_team.yml file
or opening an issue with the _add team member_ template in GitLab.

### layout

Set the layout of the blog post.
Usually it is not wanted to create and specify your own blog post
layout. In order to create your own styling we might want to
provide additional CSS via variable "additional_css" as stated below.

### title_image

Set the title_image to use.
A good blog post will have an appealing title image.
It is recommended to create a sub-folder of the images folder
`{{ site.directory.images }}` per blog post which should be named
like this: `posts/2019-10-29-my-blog-post-name/`.
If you place your _title_image_ there, providing the sub-folder and
name of the image (with extension) will be sufficient.
In this case you need to set the variable _title_image_ accordingly,
e.g. `posts/2019-10-29-my-blog-post-name/my-title_image.png`.
You can also use the value **default** for the website default
image.
If you omit the variable completely the background will fall back to
Helmholtz-blue.

### excerpt_separator

Separator to be used to indicate end of excerpt.

### categories

Set list of categories for your blog post.
The URL will contain these categories.

### tags

Set a list of tags for your blog post.

### additional_css

You can provide your own CSS file for your blog post.
You can name a CSS-file which provides custom styling to be used within your
blog post.
It is supposed to reside in `assets/css/`.
Providing the file name (including extension) is enough.

---

# Blog Post Markdown Examples

## Excerpt Separator

Use the excerpt separator stated in the frontmatter to provide a short
excerpt of the blog post and cut off the remaining text in the blog
post on the overview / preview page of the blog posts:

    Excerpt text as a preview to the blog post. Please keep this short
    and meaningful.
    <!--more-->
    Remaining text of the blog post. Go into details here.

## Headlines

Headlines and sub-headlines can be defined with leading hashes (#).

    # This is a Headline
    ## This is a Sub-Headline
    ### This is a Sub-Sub-Headline

## List Items

Bullet points can be added by asterisks (*), while numberings are done
by a number followed by a dot (1.).

Examples:

    * item one
    * item two
    1. first item
    2. second item

* item one
* item two
1. first item
2. second item

Sub-lists can be achieved by indentation:

    * item one
    * item two
      * sub-item one

* item one
* item two
  * sub-item one

## Task Lists

You can define checkboxes by squared brackets, e.g.

    * [ ] First task list item
    * [X] Second task list item (checked)
    1. [ ] First numbered task list item
    2. [X] Second numbered task list item (checked)

* [ ] First task list item
* [X] Second task list item (checked)
1. [ ] First numbered task list item
2. [X] Second numbered task list item (checked)

## Font Formatting

For italic characters embed them in underscores
(like this `_my italic text_` which results in _my italic text_).
For bold characters embed them in double asterisks
(like this `**my bold text**` which results in **my bold text**).

## Math

There is syntax for inline math as well as math on separate lines.

These are two examples:

This is an inline math snippet `$$ a^2+b^2=c^2 $$`: $$ a^2+b^2=c^2 $$

Math on a separate line can be implemented like this:

    $$
    \begin{align*}
      & \phi(x,y) = \phi \left(\sum_{i=1}^n x_ie_i, \sum_{j=1}^n y_je_j \right)
      = \sum_{i=1}^n \sum_{j=1}^n x_i y_j \phi(e_i, e_j) = \\
      & (x_1, \ldots, x_n) \left( \begin{array}{ccc}
          \phi(e_1, e_1) & \cdots & \phi(e_1, e_n) \\
          \vdots & \ddots & \vdots \\
          \phi(e_n, e_1) & \cdots & \phi(e_n, e_n)
        \end{array} \right)
      \left( \begin{array}{c}
          y_1 \\
          \vdots \\
          y_n
        \end{array} \right)
    \end{align*}
    $$

$$
\begin{align*}
  & \phi(x,y) = \phi \left(\sum_{i=1}^n x_ie_i, \sum_{j=1}^n y_je_j \right)
  = \sum_{i=1}^n \sum_{j=1}^n x_i y_j \phi(e_i, e_j) = \\
  & (x_1, \ldots, x_n) \left( \begin{array}{ccc}
      \phi(e_1, e_1) & \cdots & \phi(e_1, e_n) \\
      \vdots & \ddots & \vdots \\
      \phi(e_n, e_1) & \cdots & \phi(e_n, e_n)
    \end{array} \right)
  \left( \begin{array}{c}
      y_1 \\
      \vdots \\
      y_n
    \end{array} \right)
\end{align*}
$$

## Quotes

Use block-quotes to highlight quotes like this:

For one-line quotes put a "greater than"-sign (`>`) infront of the quote:

    > This is a one-line quote.

> This is a one-line quote.

For multi-line quotes put "greater than"-signs (`>`) above and
below the quote, one per qutation level:

    >
    This is a multi-line quote:
    Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
    sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
    aliquyam erat, sed diam voluptua.
    >> There can be second level quotes.
    >

>
This is a multi-line quote:
Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
aliquyam erat, sed diam voluptua.
>> There can be second level quotes.
>

The same look is achieved with HTML `blockquote` tags.

## Code

Use inline code with single back-tics:

    `$ apt-get update`

`$ apt-get update`

Use three back-tics and specify the language (```shell)
to mark code snippets on separate lines:

    ```shell
    $ apt-get update
    ```

```shell
$ apt-get update
```

## Images

Images can be included by stating URL, title and alternative text
like so:

    ![Alternative Image Text]({% raw %}{{ site.directory.images | relative_url }}{% endraw %}HIFIS_Logo_cropped.svg "Image Title Text")

![Alternative Image Text]({{ site.directory.images | relative_url }}HIFIS_Logo_cropped.svg "Image Title Text")

Likewise to the `title_image` variable in the _frontmatter_ it is
recommended to create a sub-folder of the images folder
`{{ site.directory.images }}` per blog post.
It should be named in the form `posts/YYYY-MM-DD-my-blog-post-name/`.
If you consequently put your images into the sub-folder
`{{ site.directory.images }}posts/2019-10-29-my-blog-post-name/`,
you need to provide the full relative path to these images using the
_Jekyll Liquid_ variable `site.directory.images`.
For example:

`{% raw %}{{ site.directory.images | relative_url }}{% endraw %}posts/2019-10-29-my-blog-post-name/my-blog-post-image.png`.

## Links

If you would like to give a link for your references you need to
specify a link text, the URL as well as link title, e.g.

    [Text for link to google.com](https://www.google.com "Link to Google!")

[Text for link to google.com](https://www.google.com "Link to Google!")

## Tables

Tables are a bit more complicated.
You need to give headers for your columns embedded in pipe-characters
(|), a separator row with dashes (-) and the cells with your content
embedded in pipe-characters (|) again:

    | column 1 | column 2 |
    |---|---|
    | row 1 | row 1 |
    | row 2 | row 2 |

| column 1 | column 2 |
|---|---|
| row 1 | row 1 |
| row 2 | row 2 |

## Custom CSS Styles for Paragraphs

To add custom CSS classes to your paragraph put a colon followed by dot and CSS
class name in curly braces.
You can use all classes from the _bootstrap 4.0_ framework, _fontawesome-solid_,
your own classes provided by the _additional_css_ variable and a couple of
custom built-in CSS classes:

### summary

Puts a blue border around the paragraph.

    {:.summary}
    This is text which appears in a box with blue border.

{:.summary}
This is text which appears in a box with blue border.

### treat-as-figure

This is a workaround for Markdowns shortcoming regarding figure environments.
It styles text within the following paragraph as if it were an image caption.
This is usually combined with an image, table or other figures.
For the image URL you should use _Jekyll Liquid_ variables
`site.url` and `site.directory.images` instead of hard-coding the URL.

### float-left

Floats the paragraph to the left side of the page and reduce its maximum width
to 30%. Useful for small images, it can be combined with `treat-as-figure`.

Example:

    My paragraph above image which is treated as normal text.

    {:.treat-as-figure}
    {:.float-left}
    My text above the image which is interpreted as caption.
    ![Alternative Image Text]({% raw %}{{ site.directory.images | relative_url }}{% endraw %}HIFIS_Logo_cropped.svg "Image Title Text")

    My paragraph below image which is treated as normal text …

My paragraph above image which is treated as normal text.

{:.treat-as-figure}
{:.float-left}
My text above the image which is interpreted as caption.
![Alternative Image Text]({{ site.directory.images | relative_url }}HIFIS_Logo_cropped.svg "Image Title Text")

My paragraph below image which is treated as normal text. It flows around the
image. Even though this is a rather hackish approach to getting more advanced
styling into Markdown it is incredibly powerful and versatile, especially when
combined with custom CSS.

Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At
vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit
amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam
et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
sanctus est Lorem ipsum dolor sit amet.

# References

For more detailed explanations of GitLab and Kramdown Markdown Syntax
take a look here:

* [GitLab Markdown Syntax Explained](https://docs.gitlab.com/ce/user/markdown.html "GitLab Markdown Syntax Documentation")
* [Kramdown Markup Syntax Explained](https://kramdown.gettalong.org/syntax.html "Kramdown Markdown Syntax Documentation")
