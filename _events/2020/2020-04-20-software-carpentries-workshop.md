---
title: Software Carpentry Workshop
layout: event
organizers:
  - dolling
  - "Bezaye Tesfaye"
lecturers:
  - dolling
  - "Bezaye Tesfaye"
type:   workshop
start:
    date:   "2020-04-21"
    time:   "09:00"
end:
    date:   "2020-04-21"
    time:   "18:00"
location:   "Online via Zoom"
fully_booked_out: false
# registration_link: https://schedule.tii.tu-dresden.de/event/26/
registration_period:
    from:   "2020-02-24"
    to:     "2020-03-20"
excerpt:
    "This basic Software Carpentry workshop will teach Shell, Git and Python for scientists and PhD students."
---

## Goal

Introduce scientists and PhD students to a powerful toolset to enhance their
research software workflow.

## Content

A Software Carpentry workshop is conceptualized as a two-day event that covers
the basic tools required for a research software workflow:

* Introduction into the _Python_ programming language

Details can also be found directly at the
[GFZ event page](https://swc-bb.git-pages.gfz-potsdam.de/swc-pages/2020-04-20-Potsdam-Berlin/).


## Requirements

Neither prior knowledge nor experience in those tools is needed.
A headset (or at least headphones) is required.
Two monitors are recommended.
