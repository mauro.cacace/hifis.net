---
title: "Helmholtz Hacky Hour #32"
layout: event
organizers:
  - erxleben
type:      hacky-hour
start:
  date:   "2021-07-14"
  time:   "14:00"
end:
  time:   "15:00"
location:
  campus: "Online"
  room:   "<a href=https://meet.gwdg.de/b/max-93j-2ef><i class=\"fas fa-external-link-alt\"></i> meet.gwdg.de</a>"
excerpt:  "<strong>Tools for handling (parametric) 3D-models</strong> Plotting in 2D is nice, but sometimes it is simply not enough."
---
## Tools for handling (parametric) 3D-models
The Hacky Hour is intended to discuss and learn about different topics in the context of research software development. During the session, you will meet Helmholtz researchers from different fields and have the opportunity to present your favorite tools and techniques. If you want to join, share your experience or have any question on the topic, let us and others know about it in [the meta pad](https://pad.gwdg.de/0HczFKgqS_C9L1QGzfpbJA#).

Creating 3D models from data sets or from scratch, manipulating them and making them interactive is a long journey through tough terrain. But the results are absolutely worth it.

We are looking forward to seeing you!
